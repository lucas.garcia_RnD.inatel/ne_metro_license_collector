import os

from NetmikoFunctions import get_grep, load_csv
from NetmikoFunctions import run_attempt
from GeneralFunctions import get_passwords

import logging
# limpando log
os.remove('logger/Netmiko.log')
logging.basicConfig(filename='logger/Netmiko.log', level=logging.DEBUG)
logger = logging.getLogger("netmiko")

''' rodar processo geral'''
if __name__ == '__main__':
    # carregando passwords da configuração
    passwords = get_passwords()

    # coletando grep de elementos rede fusion
    ''' get grep coleta'''
    ufs = ['rj']
    vendors = ['HUAWEI']

    # processando coleta em todos os elementos
    try:
        for uf in ufs:
            # coleta grep da região
            get_grep(uf, vendors, passwords, reset_process=True)  # mudar ultimo argumento para True quando quiser fazer a re-coleta!

            # tentativa 1
            vendors = ['HUAWEI']
            run_attempt(passwords, 1, vendors)

            # subindo CSV
            load_csv(passwords)

    except Exception as e:

        print(f'Error {e} \nThen Finish')
