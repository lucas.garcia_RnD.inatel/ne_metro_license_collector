import pandas as pd
import os
from datetime import datetime

def license_parse_init(log_output,ne_name):

    # define pasta do csv
    basepath = os.path.dirname(__file__)
    media_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(media_path, "csv"))

    lines = log_output.split('\n')

    init = [i for i, s in enumerate(lines) if 'Item name ' in s][0] + 3
    lines = lines[init:-2]

    filtered_lines = [segments.split()[:4] for segments in lines]
    license_table_df = pd.DataFrame(filtered_lines, columns=['Item_name', 'Item_type', 'Value', 'Description'])
    # >>>> INSERE DATA NO DATAFRAME
    now = datetime.now().date().strftime("%Y-%m-%d")
    license_table_df.insert(0, 'NE_NAME', ne_name)
    license_table_df = license_table_df.assign(DATE=now)

    # 'NE_NAME', 'Item_name', 'Item_type', 'Item_type', 'Description', 'DATE'

    current_csv_path = os.path.abspath(os.path.join(csv_path, 'license_table.csv'))
    license_table_df.to_csv(current_csv_path, mode='a', header=False, index=False)


def license_parse(log_output,ne_name):

    # define pasta do csv
    basepath = os.path.dirname(__file__)
    media_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(media_path, "csv"))

    try:
        lines = log_output.split('\n')
        no_itens = [i for i, s in enumerate(lines) if 'No license is activated' in s]
        init_itens = [i for i, s in enumerate(lines) if 'Sale name' in s]
        fin_itens = [i for i, s in enumerate(lines) if 'Description' in s]
        esn_line = [i for i, s in enumerate(lines) if 'ESN of master:' in s]
        if len(init_itens)==0:
            init_itens = [i for i, s in enumerate(lines) if 'Item name' in s]
            sale_name = False
        else:
            sale_name = True

        #get esn
        esn = lines[esn_line[0]].split(':')[-1]
        esn = esn.strip()

        # empty lists
        df_list = []

        for i, item in enumerate(init_itens):
            lines_process = lines[item:fin_itens[i]+1]
            split_list = []
            for inner_line in lines_process:
                result = inner_line.split(':')[-1]
                split_list.append(result.strip())
            # condição especial para caixa que não possui o campo "Sale_name"
            if not sale_name:
                split_list.insert(0,'-')
            df_list.append(split_list)
        #quando não houver licença ativada
        if len(no_itens) != 0:
            df_list = [['No License Activated', 'No License Activated', 'No License Activated',
                       0, 0, 'No License Activated', 'No License Activated', 0, 'No License Activated']]

        license_table_df = pd.DataFrame(df_list, columns=['Sale_name', 'Item_name', 'Item_type', 'Control_value', 'Used_value',
                                                     'Item_state', 'Item_expired_date', 'Item_trial_days', 'Description'])

        # >>>> INSERE DATA NO DATAFRAME
        now = datetime.now().date().strftime("%Y-%m-%d")
        license_table_df.insert(0, 'NE_NAME', ne_name)
        license_table_df = license_table_df.assign(ESN=esn)
        license_table_df = license_table_df.assign(DATE=now)
        license_table_df['ESN'] = license_table_df['ESN'].astype('str')

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'license_table.csv'))
        license_table_df.to_csv(current_csv_path, mode='a', header=False, index=False)
    except:
        raise Exception("Error Handling license Parse!")