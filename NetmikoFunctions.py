import os
import time

import pandas as pd
from datetime import datetime
import mysql.connector as mysql
from sqlalchemy import create_engine
from netmiko import ConnectHandler
from netmiko import redispatch
import platform

from GeneralFunctions import more_screen
from NetmikoConnections import netmiko_ssh, netmiko_telnet
from NetmikoParser import license_parse


def collect_ne(passwords, restart_auto_process):
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/tx_license_param'
    db_connection = create_engine(db_connection_str)
    if restart_auto_process:
        db = mysql.connect(
            host="10.26.82.160",
            user=passwords['sql_user'],
            passwd=passwords['sql_password'],
            database=f'tx_license_param',
            allow_local_infile=True
        )
        #DEFINE PASTA BACKUP PARA ULTIMO AUTOMATION
        df = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)
        now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        basepath = os.path.dirname(__file__)
        media_path = os.path.abspath(os.path.join(basepath, "media"))
        csv_path = os.path.abspath(os.path.join(media_path, "csv"))
        backup_path = os.path.abspath(os.path.join(csv_path, 'backup', f'AUTOMATION_PROGRESS_{now}.csv'))
        df.to_csv(backup_path, mode='w', header=True, index=False)

        #limpa tabela automation_progess
        cursor = db.cursor()
        exclude = 'truncate AUTOMATION_PROGRESS'
        cursor.execute(exclude)

    df = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)

    return df

def get_grep(uf, vendors, passwords, reset_process=False):

    df_sites = collect_ne(passwords, reset_process)
    todo = df_sites.empty
    if todo:
        # conectando no jump
        # definindo parametros do jumpserver para grep de elementos
        jump_server = {
            'device_type': 'linux',
            'ip': passwords['gesa1_host'],
            'username': passwords['gesa1_user'],
            'password': passwords['gesa1_password'],
            'port': 22
        }
        # conectando ao jump server
        jump_server_connection = ConnectHandler(**jump_server)
        # print(f"Jump server prompt: {jump_server_connection.find_prompt()}")

        # ======
        try:
            print('### GREP COLLECT START! ###')
            start = datetime.now()
            grep_text = f'cat /etc/hosts |egrep "i-br-{uf}-|m-br-{uf}" '
            grep_response = jump_server_connection.send_command_timing(grep_text, delay_factor=10)
            grep_response = grep_response.split('\n')
            filtered_lines = [x for x in grep_response if x.strip().__contains__('-br-')]
            filtered_lines = [segments.split()[:3] for segments in filtered_lines]
            df = pd.DataFrame(filtered_lines, columns=['IP', 'Name', 'Vendor'])
            accepted_patterns = ['gwc', 'gwd', 'gws', 'hl4', 'hl5', 'hl5g', 'hl5s', 'hl5d']
            pattern = '|'.join(accepted_patterns)
            df = df[df['Name'].str.contains(pattern, na=True)]
            # >>>> filtra equipamentos pelo VENDOR
            df['Vendor'] = df['Vendor'].str.upper()
            df = df[df['Vendor'].isin(vendors)]
            df.dropna(subset=['Vendor'], inplace=True)
            # >>>> filtra equipamentos pelo VENDOR
            # >>>> INSERE DATA NO DATAFRAME
            now = datetime.now().date().strftime("%Y-%m-%d")
            df['DATE'] = now
            df.drop_duplicates(inplace=True)
            # >>>> GERANDO QUERY PARA INSERIR ELEMENTOS COLETADOS PELO GREP NA TABELA AUTOMATION_PROGRESS
            query = f'INSERT INTO AUTOMATION_PROGRESS (IP, NE_NAME, VENDOR, STARTED) VALUES'
            for index, row in df.iterrows():
                query = query + f'("{row["IP"]}","{row["Name"]}", "{row["Vendor"]}", "{now}"), '

            query = query[:-2]
            db = mysql.connect(
                host="10.26.82.160",
                user=passwords['sql_user'],
                passwd=passwords['sql_password'],
                database=f'tx_license_param',
                allow_local_infile=True
            )
            cursor = db.cursor()
            # >>>> INSERINDO NOVOS ELEMENTOS NA TABELA
            cursor.execute(query)
            db.commit()
            # desconecta do jump-server
            jump_server_connection.disconnect()
            # verifica quanto tempo demorou para rodar
            final = datetime.now()
            delta = final - start
            delta = str(delta).split('.')[0]
            print('### GREP COLLECT FINISH! ###')
            print(f'Done: in {delta}')

        except:
            print('### GREP COLLECT FAIL! ###')
            jump_server_connection.disconnect()

def df_todo_retry_fn(passwords):

    '''função para tentativa extra em sites com status Ativado no SMTX '''

    # conexão com bd para rodar query de update
    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=f'tx_license_param',
        allow_local_infile=True
    )
    cursor = db.cursor()

    # conexão com bd para buscar lista de sites com status False no colected e Ativado no SMTX
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160'
    db_connection = create_engine(db_connection_str)
    df_to_fix = pd.read_sql("SELECT auto.ip, auto.NE_NAME, auto.vendor, auto.COLLECTED, param.Status_SMTX FROM "
                            "tx_license_param.AUTOMATION_PROGRESS as auto left join tx_ne_param.NE_SMTX as param on "
                            "param.NE_NAME = auto.NE_NAME where auto.COLLECTED = 'False' "
                            "and param.Status_SMTX = 'Ativado' group by 1;", con=db_connection)
    df_empty = df_to_fix.empty
    if not df_empty:
        # se houver sites neste estado um loop para mudança do status é rodado
        for index, row in df_to_fix.iterrows():
            ne_name = row['NE_NAME']
            query = f"UPDATE tx_license_param.AUTOMATION_PROGRESS SET COLLECTED = NULL, OM_LINK = NULL  where NE_NAME = '{ne_name}';"
            cursor.execute(query)
            db.commit()
    # caso contrário ele tentara mais uma vez em todos os sites que deram falha
    else:
        query = f"UPDATE tx_license_param.AUTOMATION_PROGRESS SET COLLECTED = NULL, OM_LINK = NULL where COLLECTED = 'False';"
        cursor.execute(query)
        db.commit()


def get_df_todo(gesa, passwords, vendors,retry):

    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/tx_license_param'
    db_connection = create_engine(db_connection_str)
    if not retry:
        df_todo = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)
    else:
        df_todo_retry_fn(passwords)
        df_todo = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)

    # se for teste
    teste = False

    if not teste:

        #filtra elementos por vendor
        df_todo = df_todo[df_todo['VENDOR'].isin(vendors)]

        # filtra elementos que não foram coletados ainda
        df_todo = df_todo[df_todo['COLLECTED'].isna()]

        # filtra elementos em que a tentativa é menor ou igual a atual
        df_todo['COL_ATTEMPT'] = df_todo['COL_ATTEMPT'].astype(int)
        df_todo = df_todo[df_todo['COL_ATTEMPT'] <= int(gesa)]


    else:
        # testes elementos huawei FUSION RJ
        ne_names = ['m-br-df-bsa-emq-gws-01','m-br-df-bsa-emq-gws-01','m-br-df-bsa-emt-gws-01']

        df_todo = df_todo[df_todo['NE_NAME'].isin(ne_names)]

    return df_todo

# def normalize_data():
#
#


def write_read_command(command,jump_server_connection,pattern,time_out):
    # jump_server_connection.write_channel(command + '\r\n')
    page = jump_server_connection.send_command_timing(command)
    # page = jump_server_connection.send_command(command)
    output = ''
    while time_out >= 0:
        try:
            output += str(page + '\n').replace(chr(8), '')
            if 'More' in page:
                jump_server_connection.write_channel(' ')
                page = jump_server_connection.read_until_pattern(f'More|{pattern}')
            elif pattern in page:
                return output

        except:
            time.sleep(1)
            time_out -= 1
            continue

    return None

# rodar processo por tentativa/gesa
def run_attempt(passwords,gesa,vendors,retry=None,definitive=False):
    # dict para redispatch
    redispatch_dict = {'HUAWEI_SSH': 'huawei',
                       'TELLABS_SSH': 'coriant',
                       'NOKIA_SSH': 'nokia_sros',
                       'CISCO_SSH': 'cisco_ios',
                       'HUAWEI_TELNET': 'huawei_telnet',
                       'TELLABS_TELNET': 'huawei_telnet',
                       'NOKIA_TELNET': 'nokia_sros_telnet',
                       'CISCO_TELNET': 'cisco_ios_telnet',
                       'HUAWEI': 'huawei',
                       'TELLABS': 'huawei',
                       'NOKIA': 'nokia_sros',
                       'CISCO': 'cisco_ios',
                       }

    # conectando no BACOs
    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=f'tx_license_param',
        allow_local_infile=True
    )

    db.autocommit = True
    cursor = db.cursor()

    #define sites para coleta
    site_list = get_df_todo(gesa,passwords,vendors,retry)

    #conectando no gesa
    jump_server = {
        'device_type': 'linux',
        'ip': passwords[f'gesa{gesa}_host'],
        'username': passwords[f'gesa{gesa}_user'],
        'password': passwords[f'gesa{gesa}_password'],
        'default_enter':'\r\n',
        'port': 22
    }

    # conectando ao jump server
    jump_server_connection = ConnectHandler(**jump_server)
    print(f"Jump server = GESA {gesa} | Retry {retry}: {jump_server_connection.find_prompt()}")
    connected = True

    #percorrendo Df com elementos para ser coletado
    total = len(site_list['NE_NAME'])
    ind = 1
    for index, row in site_list.iterrows():
        ip = row['IP']
        ne_name = row['NE_NAME']
        vendor = row['VENDOR']
        vendor = vendor.lower()

        # define query de falha
        if definitive:
            fail_query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa}," \
                    f"OM_LINK = 'False', COLLECTED = 'False' WHERE NE_NAME = '{ne_name}'"
        else:
            fail_query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa} " \
                         f"WHERE NE_NAME = '{ne_name}'"

        # se estiver desconectado conecta novamente
        if not connected:
            jump_server_connection = ConnectHandler(**jump_server)
            connected = True

        try:
            # ================= realizando tentativa de conexão =================
            print(f"Trying to connect:({vendor}) {ne_name} ({index + 1}/{total})=>", end=' ')
            # try ssh (PRINCIPAL)
            tacacs = ''
            print(f"ssh...", end=' ')
            connection, real_vendor = netmiko_ssh(gesa, jump_server_connection, passwords, ip, vendor)
            # try ssh sem tacacs
            if not connection and (vendor == 'HUAWEI' or vendor == 'CISCO'):
                tacacs = '(sem tacacs)'
                print(f"ssh{tacacs}...", end=' ')
                connection, real_vendor = netmiko_ssh(gesa, jump_server_connection, passwords, ip, vendor,
                                                      tacacs=False)
            # try TELNET
            if not connection:
                tacacs = ''
                print(f"telnet{tacacs}...", end=' ')
                connection, real_vendor = netmiko_telnet(gesa, jump_server_connection, passwords, ip, ne_name, vendor)
            # try TELNET sem tacacs
            if not connection and (vendor == 'HUAWEI' or vendor == 'CISCO'):
                tacacs = '(sem tacacs)'
                print(f"telnet{tacacs}...", end=' ')
                connection, real_vendor = netmiko_telnet(gesa, jump_server_connection, passwords, ip, ne_name, vendor,
                                                         tacacs=False)

            # ================= só continua se houver conexão ok =================
            if connection:
                print(f"Connected: {ne_name} by {connection}")
                pass
            else:
                print(f'Fail to connect:  {ne_name}')
                cursor.execute(fail_query)
                continue

            # ================= define query sucesso:
            success_query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa},  GESA = 'GESA_{gesa}'," \
            f" CONNECTED_VIA = '{connection}', OM_LINK = 'True', COLLECTED = 'True' WHERE NE_NAME = '{ne_name}'"

            # conectado no equipamento
            if real_vendor == 'HUAWEI':
                redispatch(jump_server_connection, device_type=redispatch_dict[f'{real_vendor}_{connection}'])
                # coletando log verbose
                lic_verbose_info = jump_server_connection.send_command('dis license verbose',strip_command=False)
                lic_verbose_info += '\n' + jump_server_connection.send_command('dis esn',strip_command=False)
                # processando log
                license_parse(lic_verbose_info,ne_name)

                #saindo do equipamento
                jump_server_connection.send_command('quit', expect_string=r'\$')
                # print(logout)
                redispatch(jump_server_connection, device_type='linux')
                # atualiza db
                cursor.execute(success_query)


            else:
                jump_server_connection.send_command('quit', expect_string=r'\)\?|:|\$|#|\>')
                # atualiza BD para não coletar novamente
                query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa},  GESA = 'GESA_{gesa}'," \
                        f" CONNECTED_VIA = {connection}, OM_LINK = 'True', COLLECTED = 'False' WHERE NE_NAME = '{ne_name}'"
                cursor.execute(query)
                print(f"Finished: {ne_name}")
                continue

            print(f"Finished: {ne_name}")

        except Exception as e:
            #desconectando para resetar o prompt
            jump_server_connection.disconnect()
            connected = False
            cursor.execute(fail_query)
            print(f'Fail processing:  {ne_name}')
            print(f'Cause:  {e}')

    #finalizando tentativa
    jump_server_connection.disconnect()

def load_csv(passwords):
    # define hora do processamento
    now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    print(f'Start Database upload')
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    license_table_file = os.path.abspath(os.path.join(csv_path, 'license_table.csv'))
    license_table_done_file = os.path.abspath(os.path.join(csv_path, 'done', f'license_table_{now}.csv'))


    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=f'tx_license_param',
        allow_local_infile=True
    )

    cursor = db.cursor()
    license_table_file = license_table_file.replace('\\', '/')
    cur_platform = platform.system()
    print(f'Loading license_table_file...')
    if cur_platform.upper() == 'WINDOWS':
        sql = f'LOAD DATA LOCAL INFILE "{license_table_file}" REPLACE INTO TABLE NE_LICENSE\n' \
              f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
              f'LINES TERMINATED BY \'\r\n\' ;'
    else:
        sql = f'LOAD DATA LOCAL INFILE "{license_table_file}" REPLACE INTO TABLE NE_LICENSE\n' \
              f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
              f'LINES TERMINATED BY \'\n\' ;'
    cursor.execute(sql)
    db.commit()

    os.rename(license_table_file, license_table_done_file)
    print(f'Finish Database upload')
