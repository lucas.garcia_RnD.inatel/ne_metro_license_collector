use tx_license_param;
truncate tx_license_param.AUTOMATION_PROGRESS;
insert into tx_license_param.AUTOMATION_PROGRESS
	SELECT ip, NE_NAME, vendor,
		null as COLLECTED,
		0 as COL_ATTEMPT,
		null as CONNECTED_VIA,
		null as GESA,
		null as OM_LINK,
		CURDATE() as STARTED
	FROM tx_ne_param.NE_METRO where NE_NAME like "i-br-%";