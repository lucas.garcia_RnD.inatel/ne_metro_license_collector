import os

def get_attribute(lines, attr):
    attr_result = [x for x in lines if attr in x][0]
    attr_result = attr_result.strip().split()[-1].strip()
    return attr_result

def get_passwords():
    '''define pasta padrão com arquivos de config, le e salva atributos nas variáveis'''
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath,"media"))
    general_path = os.path.abspath(os.path.join(directory_path, 'general'))
    file = os.path.abspath(os.path.join(general_path, 'passwords.txt'))

    file = open(file, 'r')
    lines = file.readlines()

    gesa1_host = get_attribute(lines, 'gesa1_host')

    gesa2_host = get_attribute(lines, 'gesa2_host')

    gesa3_host = get_attribute(lines, 'gesa3_host')

    gesa1_user = get_attribute(lines, 'gesa1_user')

    gesa2_user = get_attribute(lines, 'gesa2_user')

    gesa3_user = get_attribute(lines, 'gesa3_user')

    gesa1_password = get_attribute(lines, 'gesa1_password')

    gesa2_password = get_attribute(lines, 'gesa2_password')

    gesa3_password = get_attribute(lines, 'gesa3_password')

    sql_user = get_attribute(lines, 'sql_user')

    sql_password = get_attribute(lines, 'sql_password')

    huawei_user = get_attribute(lines, 'huawei_user')
    huawei_password = get_attribute(lines, 'huawei_password')

    cisco_user = get_attribute(lines, 'cisco_user')
    cisco_password = get_attribute(lines, 'cisco_password')

    passwords = {'gesa1_host': gesa1_host,
                 'gesa2_host': gesa2_host,
                 'gesa3_host': gesa3_host,
                 'gesa1_user': gesa1_user,
                 'gesa2_user': gesa2_user,
                 'gesa3_user': gesa3_user,
                 'gesa1_password': gesa1_password,
                 'gesa2_password': gesa2_password,
                 'gesa3_password': gesa3_password,
                 'sql_user': sql_user,
                 'sql_password': sql_password,
                 'huawei_user': huawei_user,
                 'huawei_password': huawei_password,
                 'cisco_user': cisco_user,
                 'cisco_password': cisco_password,
                 }

    return passwords



def more_screen(output,jump_server_connection,real_vendor,time_out=10):
    if real_vendor == 'huawei' or real_vendor == 'coriant':
        pattern = r"More|\>"
    else:
        pattern = r"More|#"

    while True:
        new_output = jump_server_connection.send_command(" ", expect_string=pattern,read_timeout=time_out)
        output += new_output
        if "More" in new_output:
            continue
        else:
            # all done
            break

    return output